
/**
 * Core server stuff.
 * This stuff is abstracted and shouldn't be edited unless
 * there is a server error.
 *
 * Ask Henry for permission to edit this file.
 */

var net = require("net");
var JSONStream = require('JSONStream');

var Car = require("./bot/car.js");
var Game = require("./game");
var SpeedBoy = require("./bot/speedboy.js");

var CAR_CLASS = SpeedBoy; // todo: let this be defined in command line

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var isTest = process.argv[6];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
    if (isTest != undefined && isTest === "test") {
        return send(
            {"msgType": "createRace", "data": {
                "botId": {
                    "name": botName,
                    "key": botKey
                },
                "trackName": "keimola",
                "carCount": 1
            }}
        );
    } else {
        return send(
            {"msgType": "join", "data": {
                "name": botName,
                "key": botKey
            }}
        );
    }
});

function send(json) {
    if (Game.game !== null)
        json["gameTick"] = Game.game.getTickCount();
    client.write(JSON.stringify(json));
    return client.write('\n');
};

function requestTurbo() {
    if (Car.car.isTurboAvailable()) {

        var turbo = Car.car.requestTurbo(Car.car._turboFactor, Car.car._turboDurationTicks);

        if (turbo) {
            send({
                msgType: "turbo",
                data: "YEEEEEAHHHHHHH"
            });
            Car.car._activateTurbo();
        }

    }
}

function tickChange(newTick) {
    if (newTick != Game.game.getTickCount()) {
        Game.game._setTickCount(newTick);
        Car.car.tick(newTick);
        requestTurbo();
    }
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    if (data.msgType === 'carPositions') {
        tickChange(data.gameTick);

        // update car stuff
        for (i in data.data) {
            var car = data.data[i];
            if (car.id.color == Car.car.getColor()) {
                Car.car._setSlipAngle(car.angle);
                Car.car._setPieceIndex(car.piecePosition.pieceIndex);
                Car.car._setPieceDistance(car.piecePosition.inPieceDistance);
                Car.car._setLaneIndices(car.piecePosition.lane.startLaneIndex, car.piecePosition.lane.endLaneIndex);
                Car.car._setLaps(car.piecePosition.lap);
                break;
            }
        }

        // handle lane switching:
        // if (Car.car.getCurrentPiece().isSwitch()) {// && Car.car._canSwitch()) {
        var laneSwitch = Car.car.requestSwitchLane();

        if (laneSwitch == 1) {
            // go left
            send({
                msgType: "switchLane",
                data: "Left"
            });
            Car.car._setSwitched(Car.car._pieceIndex);
            return;
        } else if (laneSwitch == 2) {
            // go right
            send({
                msgType: "switchLane",
                data: "Right"
            });
            Car.car._setSwitched(Car.car._pieceIndex);
            return;
        }
        // }

        // handle throttling:
        var throttle = Car.car.requestThrottle();

        // throttle isn't a number, go to a safe throttle amount
        if (isNaN(throttle)) {
            throttle = 0.5;
            console.log("Warning: throttle returned isn't a number!")
        }

        // clamp the number to be between 0 and 1
        if (throttle < 0) {
            throttle = 0
            console.log("Warning: throttle returned is less than 0!")
        } else if (throttle > 1) {
            throttle = 1
            console.log("Warning: throttle returned is greater than 1!")
        }

        send({
            msgType: "throttle",
            data: throttle
        });
        Car.car._setThrottle(throttle);
    } else if (data.msgType === 'turboAvailable') {
        data.data.turboDurationTicks = data.data.turboDurationTicks || Math.floor(data.data.turboDurationMilliseconds / 60)
        Car.car._turboIsAvailable(data.data.turboDurationTicks, data.data.turboFactor);

        var turbo = Car.car.requestTurbo(data.data.turboFactor, data.data.turboDurationTicks);

        if (turbo) {
            send({
                msgType: "turbo",
                data: "YEEEEEAHHHHHHH"
            });
            Car.car._activateTurbo();
        } else {
            send({
                msgType: "ping",
                data: {}
            });
        }
    } else {
        if (data.msgType === 'join') {
            console.log('Joined')
        } else if (data.msgType === 'yourCar') {
            Car.car = new CAR_CLASS(data.data.color);
            console.log("Yo! " + data.data.color);
        } else if (data.msgType === 'gameInit') {
            Game.game = new Game(data.data.race);
        } else if (data.msgType === 'gameStart') {
            console.log('Race started');
            Game.game._setTickCount(0);
            Car.car._setThrottle(0);
            Car.car.justSpawned(false);
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
            Car.car.justEnded();
        } else if (data.msgType === 'crash') {
            console.log(data.data.name + ' crashed!');
            if (data.data.color == Car.car.getColor()) {
                Car.car._setThrottle(0);
                Car.car.justCrashed();
            }
        } else if (data.msgType === 'spawn') {
            console.log(data.data.name + ' respawned!');
            if (data.data.color == Car.car.getColor()) {
                Car.car._setThrottle(0);
                Car.car.justSpawned(true);
            }
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});
