
var Datastore = require('nedb'),
    db = new Datastore({filename: 'data/stats', autoload: true});

var fs = require("fs");
var brain = require("brain");

var net = new brain.NeuralNetwork();

var data = [];

db.find({}, function(err, docs) {
    for (var i = 0; i < docs.length; i++) {
        var doc = docs[i];
        var output = doc.throttle;
        doc.identifier = undefined;
        doc.throttle = undefined;
        data.push({
            input: {
                distLeft: doc.distLeft / 2000,
                velocity: doc.velocity / 20,
                // radiusCurPiece: doc.radiusCurPiece / 500,
                // angleCurPiece: (doc.angleCurPiece + 90) / 90,
                // radiusNextPiece: doc.radiusNextPiece / 500,
                // angleNextPiece: (doc.angleNextPiece + 90) / 90,
                slipAngle: (doc.slipAngle + 90) / 90,
                // factor: doc.factor / 5,
            },
            output: {
                throttle: doc.throttle
            }
        })
    }

    net.train(data);
    fs.writeFileSync("data/traindata", JSON.stringify(net.toJSON()));
})

// var json = net.toJSON();