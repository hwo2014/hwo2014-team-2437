
var Datastore = require('nedb'),
    db = new Datastore({filename: 'data/stats', autoload: true}),
    dbTracks = new Datastore({filename: 'data/trackIds', autoload: true})

dbTracks.ensureIndex({ fieldName: 'identifier', unique: true }, function (err) {
});


db.count({}, function(err, count) {
    if (count > 0) {

        dbTracks.find({}, function(err, docs) {
            for (var i = 0; i < docs.length; i++) {
                var trackId = docs[i].identifier;

                db.find({ identifier: trackId }).sort({ velocity: -1 }).limit(1).exec(function(err, docs) {
                    var maxVel = docs[0].velocity;
                    console.log("Max vel " + maxVel + " of " + docs[0].identifier);
                    db.remove({ velocity: { $lt: maxVel - 1 }, identifier: docs[0].identifier }, {multi: true}, function(err, numRemov) {
                        console.log("removed", numRemov);
                    });
                });
            }
        });

    }
});
