
var Track = require("./track.js");

exports = module.exports = Game;

function Game(data) {
    this._tickCount = 0;
    this._track = new Track(data.track);
    this._laps = data.raceSession.laps;
}

/**
 * Update the tick count in the game.
 * This will either be determined by the server
 * or predicted. Either way, this should not be
 * changed by any cars.
 *
 * @param integer ticks
 */
Game.prototype._setTickCount = function(tickCount) {
    this._tickCount = tickCount;
}

Game.prototype.getTickCount = function() {
    return this._tickCount;
}

Game.prototype.getTrack = function() {
    return this._track;
}

Game.prototype.getLaps = function() {
    return this._laps;
}

Game.game = null;
