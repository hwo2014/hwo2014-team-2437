
exports = module.exports = Piece;

function Piece(data, track, index) {
    this._track = track;

    this._next = null;
    this._prev = null;

    this._switch = false;
    this._length = 0;
    this._radius = 0;
    this._angle = 0;

    this._index = index;

    if (data.switch && data.switch === true) {
        this._switch = true;
    }

    if (!isNaN(data.length)) {
        this._length = data.length;
    }

    if (!isNaN(data.radius)) {
        this._radius = data.radius;
    }

    if (!isNaN(data.angle)) {
        this._angle = data.angle;
    }
}

Piece.prototype.getNext = function() {
    return this._next;
}

Piece.prototype.getPrevious = function() {
    return this._prev;
}

Piece.prototype.isSwitch = function() {
    return this._switch;
}

Piece.prototype.isStraight = function() {
    if (this._length > 0) {
        return true;
    } else {
        return false;
    }
}

Piece.prototype.isTurningLeft = function() {
    if (this._angle < 0) {
        return true;
    } else {
        return false;
    }
}

Piece.prototype.isTurningRight = function() {
    if (this._angle > 0) {
        return true;
    } else {
        return false;
    }
}

Piece.prototype.getIndex = function() {
    return this._index;
}

Piece.prototype.getAngle = function() {
    return this._angle;
}

Piece.prototype.getRadius = function() {
    return this._radius;
}

function sign(x) { return x ? x < 0 ? -1 : 1 : 0; }

Piece.prototype.getLength = function(lane) {
    if (this._length > 0) {
        return this._length;
    }

    // not a straight lane
    var radius = this._radius*sign(this._angle)*-1 + lane.getDistance();

    var length = Math.abs(this._angle / 360 * 2 * Math.PI * radius);

    return length;
}
