
exports = module.exports = Lane;

function Lane(data) {
    this._distance = data.distanceFromCenter;
    this._index = data.index;

    this._left = null;
    this._right = null;
}

Lane.prototype.getDistance = function() {
    return this._distance;
}

Lane.prototype.getLeft = function() {
    return this._left;
}

Lane.prototype.getRight = function() {
    return this._right;
}
