
var Piece = require("./piece.js")
var Lane = require("./lane.js")

exports = module.exports = Track;

function Track(data) {
    this._id = data.id;
    this._pieces = [];
    this._lanes = [];

    for (i in data.pieces) {
        this._pieces.push(new Piece(data.pieces[i], this, Number(i)));
    }

    for (i in this._pieces) {
        i = Number(i);
        this._pieces[i]._prev = this._pieces[(i-1+this._pieces.length) % this._pieces.length];
        this._pieces[i]._next = this._pieces[(i+1) % this._pieces.length];
    }

    for (i in data.lanes) {
        this._lanes.push(new Lane(data.lanes[i]));
    }

    for (i in this._lanes) {
        i = Number(i);
        if (this._lanes[i-1])
            this._lanes[i]._left = this._lanes[i-1];
        if (this._lanes[i+1])
            this._lanes[i]._right = this._lanes[i+1];
    }

}

Track.prototype.getId = function() {
    return this._id;
}

Track.prototype.getPiece = function(id) {
    return this._pieces[id];
}

Track.prototype.getPieceCount = function() {
    return this._pieces.length;
}

Track.prototype.getLane = function(id) {
    return this._lanes[id];
}

Track.prototype.getLaneCount = function() {
    return this._lanes.length;
}
