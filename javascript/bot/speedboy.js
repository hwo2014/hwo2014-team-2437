
var util = require("util")
,   Car = require("./car.js")
,   Game = require("../game")
,   fs = require("fs")
;

/**
 * SpeedBoy Racing Algorithm
 */

exports = module.exports = SpeedBoy;

/**
 * Constructor
 */
function SpeedBoy(color) {
    Car.call(this, color);

    // to determine when to send a switch message
    this.lastSwitch = null;

    // to calculate distance travelled
    this.lastStoredPiece = null;
    this.lastStoredPieceDistance = 0;
    this.lastStoredLane = null;
    this.distanceTravelled = 0;

    // to calculate velocity
    this.lastdistanceTravelled = 0;
    this.velocity = 0;

    // should turbo
    this.shouldTurbo = false;
}

util.inherits(SpeedBoy, Car);

//
//  Hooks - these are called by the game, often times requesting data
//

/**
 * Called every second. You can't send any messages to the car here.
 */
SpeedBoy.prototype.tick = function(tickCount) {

    // calculating this.distanceTravelled

    // make sure these variables are valid or set to something
    this.lastStoredPiece = this.lastStoredPiece || this.getCurrentPiece();
    this.lastStoredLane = this.lastStoredLane || this.getCurrentLane();

    // the distance between this tick and the last tick
    var deltaDistance = 0;

    // if the last piece is the same as our current piece
    if (this.getCurrentPiece() == this.lastStoredPiece) {
        // distance is simple to calculate,
        // just take the current distance travelled on the piece
        // and minus it by the last distance travelled.
        deltaDistance =
            this.getPieceDistance()
            - this.lastStoredPieceDistance;

    // the last piece is not the same as our current piece...
    } else {
        // warning: this doesn't take account of lane switching in the middle
        // as well as that, switches add a length of around 2 if you do a switch
        // this will give you a slightly inaccurate distance travelled,
        // but the error is very negligible.

        // Get the distance between the last piece we were on and the end of the piece.
        var lastPieceLength = this.lastStoredPiece.getLength(this.lastStoredLane);
        deltaDistance = lastPieceLength - this.lastStoredPieceDistance;

        // Go through each piece between the last piece and our piece
        // and add the length of each piece to the distance.
        var piece = this.lastStoredPiece.getNext();
        var lane = this.lastStoredLane;
        while (piece != this.getCurrentPiece()) {
            if (piece.isSwitch()) {
                lane = this.getCurrentLane();
            }
            deltaDistance += piece.getLength(lane);
            piece = piece.getNext()
        }

        // Add the distance of where we are at on the current piece
        deltaDistance += this.getPieceDistance();
    }
    // increment distance travelled
    this.distanceTravelled += deltaDistance;

    // Now store information from this tick to use on the next tick.
    this.lastStoredPiece = this.getCurrentPiece();
    this.lastStoredPieceDistance = this.getPieceDistance();
    this.lastStoredLane = this.getCurrentLane();

    // calculate velocity
    this.velocity = this.distanceTravelled - this.lastdistanceTravelled;
    this.lastdistanceTravelled = this.distanceTravelled;

    console.log("Velocity: " + this.velocity);
}

SpeedBoy.prototype.justCrashed = function() {
    console.log("I just crashed!");
}

SpeedBoy.prototype.justSpawned = function(respawn) {
    if (!respawn) {
        console.log("I just spawned!");
    } else {
        console.log("I just respawned!");
    }
}

SpeedBoy.prototype.justEnded = function() {
    console.log("End match!");
}

/**
 * The server requests the throttle amount
 *
 * @return {number} throttle amount
 */
SpeedBoy.prototype.requestThrottle = function() {
    var piece = this.getCurrentPiece();
    var lane = this.getCurrentLane();

    var dist = this.getPieceDistance();
    var distLeft = 0;
    if (piece.isStraight()) {
        distLeft += piece.getLength(lane) - dist;
    }

    var reachingGoal = false;

    piece = piece.getNext();

    if (piece.getIndex() == 0) {
        reachingGoal = true;
    }

    while (piece.isStraight())
    {
        if (piece.getIndex() == 0) {
            reachingGoal = true;
        }
        distLeft += piece.getLength(lane);
        piece = piece.getNext();
    }

    var DISTANCE_FROM_A_TURN_UNTIL_SLOW_DOWN = 15;
    var MAX_VELOCITY_ON_ANGLE = 1.65;
    var ACCELERATION = 0.26; // guesswork, not correct
    var DEACCELERATION = -0.26; // guesswork, not correct

    var angle = Math.abs(piece.getAngle()) // angle of the curve
    var maxVelocity = Math.log((90 - angle) / 90 * 100) * MAX_VELOCITY_ON_ANGLE; // max velocity on curve
    var distanceFromTurn = this.velocity / MAX_VELOCITY_ON_ANGLE * DISTANCE_FROM_A_TURN_UNTIL_SLOW_DOWN; // determine the distance from the curve that we'll use to slow down

    var factor;
    if (this.isUsingTurbo()) {
        factor = this.getTurboFactor();
    } else {
        factor = 1;
    }

    if (Game.game.getLaps()-1 == this.getLaps() && reachingGoal) {
        if (this.isTurboAvailable() && this.getCurrentPiece().isStraight()) {
            this.shouldTurbo = true;
        }

        return 1.0;
    }

    var throttle = 0.5

    // if we're far enough from a curve, and our turbo is available,
    // then lets use our turbo
    if (distLeft > distanceFromTurn * 4 && this.isTurboAvailable() && this.getCurrentPiece().isStraight()) {
        this.shouldTurbo = true;
    } else {
        this.shouldTurbo = false;
    }

    if (this.getCurrentPiece().isStraight()) {

        // I'm mostly using this equation:
        // v^2 = v0^2 + 2*a(y - y0)

        // this is our predicted velocity (if we were to constantly break)
        var predictedVelocitySqrd = this.velocity*this.velocity + 2*DEACCELERATION*distLeft;
        var predictedVelocity;
        if (predictedVelocitySqrd <= 0) {
            predictedVelocity = 0;
        } else {
            predictedVelocity = Math.sqrt( predictedVelocitySqrd );
        }

        if (predictedVelocity < maxVelocity) {
            // So what is the maximum velocity we can go on this straight piece
            // so that we could reach the maximum velocity for the oncoming
            // curved piece?
            //
            // v0^2 = v^2 - 2*a(y - y0)
            var guessedMaxVelocitySqrd = maxVelocity*maxVelocity + 2*ACCELERATION*distLeft;
            var guessedMaxVelocity;
            if (guessedMaxVelocitySqrd <= 0) {
                guessedMaxVelocity = 0;
            } else {
                guessedMaxVelocity = Math.sqrt( guessedMaxVelocitySqrd );
            }

            console.log(guessedMaxVelocity);

            if (this.velocity > guessedMaxVelocity) {
                // breaking mechanism!
                //
                // I don't know what the throttle does to our acceleration
                // this will reduce throttle based on velocity.
                // the higher our velocity is, the lower the throttle will be
                throttle = 0.5 - (this.velocity / 5 * 0.5)
                if (throttle < 0) {
                    throttle = 0;
                }
            } else {
                throttle = 1;
            }

        // we're screwed, brake!!
        } else {
            throttle = 0;
        }

    // if we're on a curve
    } else {
        // looks like our velocity is greater than the stable velocity
        if (this.velocity > maxVelocity) {
            // breaking mechanism!
            //
            // I don't know what the throttle does to our acceleration
            // this will reduce throttle based on velocity.
            // the higher our velocity is, the lower the throttle will be
            throttle = 0.5 - (this.velocity / 5 * 0.5)
            if (throttle < 0) {
                throttle = 0;
            }
        } else {
            var slipAngle = Math.abs(this.getSlipAngle())

            // looks like we're about to slip, I think...
            // pretty arbitrary, can't tell if this works.
            if (angle - 5 < slipAngle) {
                // slow down!
                throttle = 0.5;
            } else {
                // otherwise, speed up!
                throttle = 1.0;
            }
        }

        if (this.isUsingTurbo()) {
            // If we're turboing it will multiply our throttle by a factor.
            // This might mess up our calculations, so we divide the throttle
            // by the factor.
            throttle = throttle / this.getTurboFactor();
        }
    }

    return throttle;
}

var STAY = 0,
    LEFT = 1,
    RIGHT = 2

/**
 * The server asks if you want to switch lanes
 * Be warned, if you switch lanes then `requestThrottle` won't
 * be called, and the throttle can't be changed.
 *
 * @return {number} the direction to switch to
 */
SpeedBoy.prototype.requestSwitchLane = function() {

    // This algorithm will find the all the turns between switch pieces
    // And goes to the one with the least length.

    // Find a switch piece that we might be approaching
    // It isn't the switch we're currently on!
    var piece = this.getCurrentPiece().getNext();
    while (!piece.isSwitch()) {
        piece = piece.getNext();
    }

    // We're determining if we are going to turn for only one switch piece.
    // If we keep recalculating for the same switch, then it will be a waste
    // of resources.
    // These two lines ensure that we only calculate which lane to switch to
    // Once per switch piece.
    if (piece == this.lastSwitch) { return STAY; }
    this.lastSwitch = piece;

    // We have three lanes we can travel on
    // The one we're currently on,
    // the one to the left of us,
    // and the one to the right of us.
    var stayLength = 0;
    var leftLength = 0;
    var rightLength = 0;

    var stayLane = this.getCurrentLane();
    var leftLane = stayLane.getLeft();
    var rightLane = stayLane.getRight();

    // Find the length of each lane to determine which one has the shortest length
    do {
        piece = piece.getNext();
        if (!piece.isStraight()) {
            stayLength += piece.getLength(stayLane);
            if (leftLane != null)
                leftLength += piece.getLength(leftLane);
            else
                leftLength = Number.MAX_VALUE;

            if (rightLane != null)
                rightLength += piece.getLength(rightLane);
            else
                rightLength = Number.MAX_VALUE;
        }
    } while (!piece.isSwitch())

    // go to the lane with the least length
    // the lower the distance we need to travel, the fast we'll go.
    if (stayLength <= rightLength && stayLength <= leftLength) {
        return STAY;
    } else if (leftLane != null && leftLength < rightLength) {
        return LEFT;
    } else if (rightLane != null && rightLength < leftLength) {
        return RIGHT;
    }

    throw "BROKEN SWITCH LOGIC!";
}

/**
 * The server says you can go into turbo
 * but should you?
 * This is called directly after "tick" and "requestThrottle".
 *
 * Called every tick for 30 seconds until turbo goes away
 *
 * @param  {number} factor how much it will multiply the speed by
 * @param  {number} timeLeft how many ticks left to use this turbo
 * @return {boolean} should turbo
 */
SpeedBoy.prototype.requestTurbo = function(factor, timeLeft) {
    if (this.shouldTurbo) {
        return true;
    } else {
        return false;
    }
}

//
// Any other function you might want to add
//

