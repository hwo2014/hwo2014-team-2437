
var Game = require("../game");

/**
 * Basic car algorithm (very stupid car)
 */

exports = module.exports = Car;

/**
 * Initialize base bot
 *
 * The base bot will handle all the internal functions,
 * and the timing of the functions.
 *
 * It will not handle core AI, that will be up to
 * the SpeedBoy class.
 */
function Car(color) {
    this._color = color;

    this._turboDuration = 0;
    this._turboFactor = 0;
    this._turboAvailable = false;
    // if turbo is activated:
    this._turboExpirationDate = 0;

    this._throttle = 0;

    this._slipAngle = 0;

    this._pieceIndex = 0;
    this._pieceDistance = 0;

    this._startLaneIndex = 0;
    this._endLaneIndex = 0;

    this._switched = null;

    this._laps = 0;
}

/**
 * Get the color identifier of the car
 * @return {string} color
 */
Car.prototype.getColor = function() {
    return this._color;
}

/**
 * Check if the car is in turbo mode.
 * @return {Boolean} is it in turbo mode
 */
Car.prototype.isUsingTurbo = function() {
    if (this.amountOfTurboLeft() > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Get the amount of time the turbo has left in ticks.
 * @return {number} the amount of turbo left
 */
Car.prototype.amountOfTurboLeft = function() {
    if (this.isTurboAvailable()) {
        var turboAmount = this._turboExpirationDate - Game.game.getTickCount();
        if (turboAmount < 0) {
            return 0;
        } else {
            return turboAmount;
        }
    } else {
        return 0;
    }
}

/**
 * Get the turbo multiplication factor
 * @return {number} turbo factor
 */
Car.prototype.getTurboFactor = function() {
    return this._turboFactor;
}

/**
 * Ask if the turbo is available
 * @return {Boolean} the availability of turbo
 */
Car.prototype.isTurboAvailable = function() {
    return this._turboAvailable;
}

/**
 * Get the current throttle of the car (or last one if called from requestThrottle)
 * @return {number} throttle
 */
Car.prototype.getThrottle = function() {
    return this._throttle;
}

/**
 * Get the current piece
 * @return {Piece} an object representing the piece
 */
Car.prototype.getCurrentPiece = function() {
    var track = Game.game.getTrack();
    return track.getPiece(this._pieceIndex);
}

/**
 * Get the index of the current piece
 * @return {number} piece index
 */
Car.prototype.getCurrentPieceIndex = function() {
    return this._pieceIndex;
}

/**
 * Get the current lane
 * @return {Lane} an object representing the lane
 */
Car.prototype.getCurrentLane = function() {
    var track = Game.game.getTrack();
    return track.getLane(this._endLaneIndex);
}

/**
 * Get the index of the current lane
 * @return {number} lane index
 */
Car.prototype.getCurrentLaneIndex = function() {
    return this._endLaneIndex;
}

/**
 * Get the distance travelled from the start of the current piece
 * @return {number} the distance
 */
Car.prototype.getPieceDistance = function() {
    return this._pieceDistance;
}

/**
 * Can this car turn in this direction
 * @param  {constant} turn STAY, LEFT, or RIGHT
 */
Car.prototype.canTurn = function(turn) {
    if (turn == 0) {
        return true;
    } else if (turn == 1) {
        // can turn left
        if (this._endLaneIndex - 1 >= 0) {
            return true;
        } else {
            return false;
        }
    } else if (turn == 2) {
        // can turn right
        var track = Game.game.getTrack();
        if (this._endLaneIndex + 1 < track.getLaneCount()) {
            return true;
        } else {
            return false;
        }
    }
}

Car.prototype.getSlipAngle = function() {
    return this._slipAngle;
}

Car.prototype.getLaps = function() {
    return this._laps;
}

//
// HOOKS
//

Car.prototype.tick = function(tickCount) {}
Car.prototype.justSpawned = function(respawn) {}
Car.prototype.justCrashed = function() {}
Car.prototype.justEnded = function() {}
Car.prototype.requestThrottle = function() {return 1;}
Car.prototype.requestSwitchLane = function() {return 0;}
Car.prototype.requestTurbo = function(factor, timeLeft) {return true;}

//
// INTERNAL FUNCTIONS (DON'T TOUCH!)
//

Car.prototype._turboIsAvailable = function(turboDuration, turboFactor) {
    this._turboDuration = turboDuration;
    this._turboFactor = turboFactor;
    this._turboAvailable = true;
}

Car.prototype._activateTurbo = function() {
    this._turboExpirationDate = Game.game.getTickCount() + this._turboDuration;
    this._turboAvailable = false;
}

Car.prototype._setThrottle = function(throttle) {
    this._throttle = throttle;
}

Car.prototype._setSlipAngle = function(slipAngle) {
    this._slipAngle = slipAngle;
}

Car.prototype._setPieceIndex = function(pieceIndex) {
    this._pieceIndex = pieceIndex;
}

Car.prototype._setPieceDistance = function(pieceDistance) {
    this._pieceDistance = pieceDistance;
}

Car.prototype._setLaneIndices = function(startLaneIndex, endLaneIndex) {
    this._startLaneIndex = startLaneIndex;
    this._endLaneIndex = endLaneIndex;
}

Car.prototype._canSwitch = function() {
    if (this._pieceIndex !== this._switched) {
        return true;
    } else {
        return false;
    }
}

Car.prototype._setSwitched = function(piece) {
    this._switched = piece;
}

Car.prototype._setLaps = function(laps) {
    this._laps = laps;
}

Car.car = new Car("red"); // initialize with dummy car
