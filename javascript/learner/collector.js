
var Datastore = require('nedb'),
    db = new Datastore({filename: 'data/stats', autoload: true})
    dbTracks = new Datastore({filename: 'data/trackIds', autoload: true})

dbTracks.ensureIndex({ fieldName: 'identifier', unique: true }, function (err) {
});

process.maxTickDepth = 40000;

/**
 * Collects data and statistics that are relevant.
 */

exports = module.exports = Collector;

/**
 * Discussion:
 *
 * The relevant data should be:
 * - Throttle (output)
 * - Distance from next curve
 * - Velocity
 * - Radius of current piece
 * - Angle of current piece
 * - Radius of next piece
 * - Angle of next piece
 * - Slip angle
 * - ID (identify a piece and it's track, do not use this for training though)
 *
 * Not all data should go through training:
 * - Only take data with the highest velocity.
 * - Section off data by "ID"
 *
 * Random variables:
 * - ACCELERATION +/- random(0, 33) / 100
 * - DISTANCE_FROM_A_TURN_UNTIL_SLOW_DOWN +/- random(0, 15)
 * - MAX_VELOCITY_ON_ANGLE +/- random(0, 100) / 100
 *
 * Data collection / culling:
 * - Section off each piece by the road angle
 *      - I'll call these "chunks"
 * - Take a snapshot of each tick.
 * - If player crashes, remove all snapshots with the previous 2 chunks.
 *
 */

function Collector() {
    this._snapshots = [];
}

Collector.prototype.snapshot = function(data) {
    this._snapshots.push(data);
}

function sign(num) {
    return num == 0 ? 0 : (num < 0 ? -1 : 1);
}

/**
 * The car has crashed, so we need to cull the data
 */
Collector.prototype.justCrashed = function() {
    // do this two times
    for (var i = 0; i < 2; i++) {
        // get the last snapshot
        // remove all snapshots with the same angle sign (aka a chunk)
        var last = this._snapshots[this._snapshots.length];
        if (last != null) {
            var angleSign = sign(last.angleCurPiece);
            while (last != null && sign(last.angleCurPiece) == angleSign) {
                this._snapshots.pop();
                last = this._snapshots[this._snapshots.length];
            }
        }
    }

    for (var i = 0; i < this._snapshots.length; i++) {
        var snapshot = this._snapshots[i];

        dbTracks.insert({ identifier: snapshot.identifier });
        db.insert(snapshot);
    }

    this._snapshots = [];
}

Collector.prototype.save = function() {
    for (var i = 0; i < this._snapshots.length; i++) {
        var snapshot = this._snapshots[i];

        dbTracks.insert({ identifier: snapshot.identifier });
        db.insert(snapshot);
    }

    this._snapshots = [];

    this.cull();

    console.log("done collecting data!");
}

Collector.prototype.cull = function() {
    db.count({}, function(err, count) {
        if (count > 0) {

            dbTracks.find({}, function(err, docs) {
                for (var i = 0; i < docs.length; i++) {
                    var trackId = docs[i].identifier;

                    db.find({ identifier: trackId }).sort({ velocity: -1 }).limit(1).exec(function(err, docs) {
                        var maxVel = docs[0].velocity;
                        db.remove({ velocity: { $lt: maxVel - 1 }, identifier: docs[0].identifier }, {multi: true}, function(err, numRemov) {});
                    });
                }
            });

        }
    });
}
